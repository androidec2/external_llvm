LOCAL_PATH := $(call my-dir)

# For the device
# =====================================================
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
  ExecutionEngine.cpp \
  ExecutionEngineBindings.cpp \
  RTDyldMemoryManager.cpp \
  TargetSelect.cpp

LOCAL_MODULE := libLLVMExecutionEngine

LOCAL_MODULE_TAGS := optional

include $(LLVM_DEVICE_BUILD_MK)
include $(BUILD_STATIC_LIBRARY)
